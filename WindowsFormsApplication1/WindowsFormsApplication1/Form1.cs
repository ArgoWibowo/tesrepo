﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int bil1 = Int32.Parse(txtBil1.Text.ToString());
            int bil2 = Int32.Parse(txtBil2.Text.ToString());
            int result = bil1 + bil2;
            MessageBox.Show(result.ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int bil1 = Int32.Parse(txtBil1.Text.ToString());
            int bil2 = Int32.Parse(txtBil2.Text.ToString());
            int result = bil1 - bil2;
            MessageBox.Show(result.ToString());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int bil1 = Int32.Parse(txtBil1.Text.ToString());
            int bil2 = Int32.Parse(txtBil2.Text.ToString());
            int result = bil1 * bil2;
            MessageBox.Show(result.ToString());
        }
    }
}
